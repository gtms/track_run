PREFIX = /usr/local
BINDIR = $(PREFIX)/bin

all:
	@echo "Nothing to do, try \"make install\" instead."

install:
	@install -v -d "$(BINDIR)/" && install -m 0755 -v "./trun.lua" "$(BINDIR)/trun"

uninstall: trun.lua trun_status.lua
	@rm -vrf "$(BINDIR)/trun"

.PHONY: all install uninstall
