#!/usr/bin/env lua

-- Version: 1.0.0 ( 08.08.2021 )
--
-- Usage
-- <cmd> 1> >(trun <handler> <name>)
--
-- # Track run
-- TrackRun parse stdin of command and cache status to file.
-- Status is taken from handler module via `handle` function.
--
-- # Handler
-- Handler is module that implements specific functions to catch some key
-- moment of tracking. Every handler must implement 'handle' function that
-- returns status for that line. It takes two arguments:
-- 1. `userdata` is table for storing data between hooks
-- 2. `line` is current line to get status from.
--
-- Handlers must have `lua` ext. and be inside specific folder. viz->#Config
--
-- ## Handle function
-- handle function can return either table or string. When table is returned,
-- every item will end up on new line in status_file so it can be easily used
-- by other scripts.
--
-- ## Hooks
-- There are these hook functions you can implement:
-- - on_start(userdata): before first line is passed to handle function
-- - on_update(userdata, line, status): when status change
-- - on_end(userdata): when script end
--
-- ## Userdata
-- userdata has pre filled `name` key with value of trun name
--
-- # Config
-- There are 2 ENV VAR you may want to set:
-- - TRUN_HANDLERS_DIR: directory where handlers are stored (default to XDG_CONFIG_HOME/trun)
local handlers_dir = os.getenv('TRUN_HANDLERS_DIR') or os.getenv('XDG_CONFIG_HOME') .. '/trun'
-- - TRUN_STATUS_DIR: directory to store files with statuses (default to XDG_CACHE_HOME/trun)
local status_dir = os.getenv('TRUN_STATUS_DIR') or os.getenv('XDG_CACHE_HOME') .. '/trun'
--

local handler_name = arg[1]
if not handler_name then
  io.write('No handler provided!\n')
  os.exit(1)
end
local name = arg[2] or 'trun'

-- load handler
local handler_path = string.format('%s/?.lua', handlers_dir)
package.path = handler_path .. ';' .. package.path
local handler = require(handler_name)
if not handler then
  io.write('handler "' .. handler_name .. '" not found!\n')
  os.exit(1)
end
local userdata = {name = name}

-- status dir
if not io.open(status_dir) then
  os.execute('mkdir ' .. status_dir)
end

-- status file
local status_file = status_dir .. '/' .. name .. '.' .. handler_name
local file = io.open(status_file, 'w')
if not file then
  os.execute('touch ' .. status_file)
end

-- cleanup removes status file and notify handlers.
local function cleanup()
  os.execute('rm ' .. status_file)
  if handler.on_end then
    handler.on_end(userdata)
  end
end

-----------------------------------------------------------------------------------------------------------------------
-- handling signals
-----------------------------------------------------------------------------------------------------------------------

local function on_exit(signum)
  cleanup()
  os.exit(signum)
end

local signal = require 'posix.signal'
if signal then
  signal.signal(signal.SIGINT, on_exit)
  signal.signal(signal.SIGTERM, on_exit)
  signal.signal(signal.SIGKILL, on_exit)
  signal.signal(signal.SIGHUP, on_exit)
else
  io.write('For signal handle install luaposix (`luarocks install luaposix`).\n')
end

-----------------------------------------------------------------------------------------------------------------------

local status

local function update_file(output)
  file = io.open(status_file, 'w')
  if type(output) == 'table' then
    file:write(table.concat(output, '\n'))
  else
    file:write(output)
  end
  file:close()
end

if handler.on_start then
  handler.on_start(userdata)
end

local lastStatus
for line in io.lines() do
  lastStatus = status
  status = handler.handle(userdata, line)
  if status and status ~= lastStatus then
    update_file(status)
    if handler.on_update then
      handler.on_update(userdata, line, status)
    end
  end
end

cleanup()
