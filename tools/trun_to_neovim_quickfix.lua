-- Add this to your nvim folder to `plugin/trun.lua`.
--
-- use `:Trunqf <name>` to fill quickfix list with lines after last
-- success status. It has autocompletion for running truns, press <tab> to list
-- those.
--
-- !important
-- Handler needs to store the output inside file and path to that file must be
-- added as second line to status file. viz->examples/handler_tmp_output_file.lua
--
-- returns list of running truns
local complete = function()
  local dir = os.getenv('TRUN_STATUS_DIR') or os.getenv('XDG_CACHE_HOME') .. '/trun'
  local ls = io.popen('ls ' .. dir)
  local truns = {}
  for trun in ls:lines() do
    local name = trun:match('(.*)%..*')
    table.insert(truns, name)
  end
  return truns
end

-- add trun to quickfix list
-- it needs to have its tempfile for output
local to_qf = function(name)
  if not name then
    return
  end
  vim.fn.setqflist({}, 'r')
  local handle = io.popen('trun_status - ' .. name)
  local o = {}
  for line in handle:lines() do
    table.insert(o, line)
  end
  if #o == 0 then
    print('No running trun for "' .. name .. '"')
    return
  end
  -- edit this if path to tmpfile(errorfile) is on another line
  local errorfile = o[2]
  if errorfile then
    local errfile = io.open(errorfile, 'r')
    local lines = {}
    for line in errfile:lines() do
      table.insert(lines, line)
    end
    vim.fn.setqflist({}, ' ', {lines = lines})
    vim.cmd [[ copen ]]
    vim.cmd [[ normal G ]]
  else
    print('Trun for "' .. name .. '" does not have tmp file')
    return
  end
end

-- Make functions globally accessible
_G.Trun = {complete = complete, qf = to_qf}

vim.cmd [[
fun! TrunComplete(A,L,P)
    return v:lua.Trun.complete()
endfun
]]
vim.cmd [[command! -nargs=1 -complete=customlist,TrunComplete Trunqf v:lua.Trun.qf("<args>")]]
