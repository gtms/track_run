#!/usr/bin/env lua

-- Usage: trun_status.lua [-] [<name>]
--
-- Return formatted string for all truns to use inside status bar of your WM.
-- It depends on status codes to be:
-- - `0`  - running
-- - `1`  - success
-- - `-1` - error
--
-- You can change colors. viz->#Config
--
-- example output: [%{F#ff0000}NAME%{F-} %{F#00ff00}OTHERNAME%{F-}]
--
-- To ignore formatting add `-`(single dash) argument. Mostly for debugging.
-- - It will just print output of status_file
--
-- To get only single trun, add `name` as argument.
--
-- It does not print any errors. (TODO)
--
-- Config
local trun_status = os.getenv('TRUN_STATUS_DIR') or os.getenv('XDG_CONFIG_HOME') .. '/trun'
-- map status to foreground colors.
local status_map = {[0] = 'ffa500', [1] = '00ff00', [-1] = 'ff0000'}
---

-- silent fail if dir not exists
local status_dir = trun_status
if not io.open(status_dir) then
  return ''
end

local trun_name = arg[1]
local raw -- single dash argument means - no formatting, just print
if arg[1] == '-' then
  raw = true
  trun_name = arg[2]
end

-- get all status files
local status_files = {}
local list = io.popen('ls ' .. status_dir)
for f in list:lines() do
  table.insert(status_files, f)
end

-- format status_file to string
local format = function(file, name)
  local output
  if not file then
    table.insert(output, '')
  else
    if raw then
      local status = file:read('*a')
      output = status
    else
      local status = file:read('*n')
      -- Edit this to your liking output
      output = '%{F#' .. status_map[tonumber(status)] .. '} ' .. name:upper() .. ' %{F-}'
      file:close()
    end
  end
  return output
end

local result = {}
-- For every file fill out results
for _, status_file_name in pairs(status_files) do
  local name, _ = status_file_name:match('(.*)%.(.*)')
  if name then
    local status_file_path = status_dir .. '/' .. status_file_name
    local status_file = io.open(status_file_path, 'r')
    if trun_name then
      if name == trun_name then
        table.insert(result, format(status_file, name))
      end
    else
      table.insert(result, format(status_file, name))
    end
  end
end

-- print out results
if #result > 0 then
  if raw then
    print(table.concat(result, '\n'))
  else
    print('[' .. table.concat(result, ',') .. ']')
  end
end
