-- Trun advanced handler for *track status* and *cache* last output betwen status
-- changes in tmp_file. It saves path to that file in status_file on second line.
--
-- So for exmple, on error you can load this output to quickfix list
-- inside vim.
-- viz->tools/trun_to_neovim_quickfix.lua and examples/handler_simple.lua for
-- examples of neovim-remote (to notify inside neovim or you can run Trunqf to
-- load quickfix list on fly)
--
-- interface
-- - handle(userdata, line): return status
-- - (optional) on_start(userdata): call once before start reading
-- - (optional) on_update(userdata, line, status): calls every time when status changed
-- - (optional) on_end(userdata): calls once after reading end
--
local status_map = {['running'] = 0, ['success'] = 1, ['error'] = -1}

local notify =
  function(status) os.execute('notify-send "trun status changed" "' .. status .. '"') end

local function get_status(line)
  if line:find('SUCCESS') then
    return status_map.success
  elseif line:find('RUNNING') then
    return status_map.running
  else
    return status_map.error
  end
end

return {

  -- on_start(userdata): call once before start reading
  on_start = function(data)
    -- re-create tmp_file and save it to data table.
    data.tmpfile = '/tmp/' .. data.name .. '.trun'
    os.execute('rm ' .. data.tmpfile .. ' 2>/dev/null')
  end,

  -- handle(userdata, line): return status
  handle = function(line, data)
    -- write each line to tmp_file
    local tmpfile = io.open(data.tmpfile, 'a')
    tmpfile:write(line, '\n')
    tmpfile:close()
    return {get_status(line), data.tmpfile}
  end,

  -- on_update(userdata, line, status): calls every time when status changed
  on_update = function(_, status, data)
    -- on success, clear tmp_file.
    if status[1] == status_map.success then
      io.open(data.tmpfile, 'w'):close()
    end
    notify()

    -- viz->examples/handler_simple.lua for other examples wit neovim-remote
  end,

  -- on_end(userdata): calls once after reading end
  on_end = function(data)
    notify()
    -- remove tmp_file
    os.execute('rm ' .. data.tmpfile)
  end,
}
