--- Trun simple handler for just status of command
--
-- interface
-- - handle(userdata, line): return status
-- - (optional) on_start(userdata): call once before start reading
-- - (optional) on_update(userdata, line, status): calls every time when status changed
-- - (optional) on_end(userdata): calls once after reading end
--
local status_map = {['running'] = 0, ['success'] = 1, ['error'] = -1}

-- send nvim_cmd to all neovim instances
local nvim_cmd = function(nvim_cmd)
  local servers = io.popen('nvr --serverlist')
  for socket in servers:lines() do
    local cmd = string.format('nvr --servername "%s" -cc "%s" --nostart -s &', socket, nvim_cmd)
    print(cmd)
    os.execute(cmd)
  end
  servers:close()
end

return {
  -- handle(userdata, line): return status
  handle = function(_, line)
    if line:find('SUCCESS') then
      return status_map.success
    elseif line:find('RUNNING') then
      return status_map.running
    else
      return status_map.error
    end
  end,

  -- on_update(userdata, line, status): calls every time when status changed
  on_update = function(data, _, status)

    -- you can send linux notifications
    os.execute(string.format('notify-send "trun status changed for %s" "%s"', data.name, status))

    -- or you can run neovim command to trigger neotification.
    -- You need neovim-remote (https://github.com/mhinz/neovim-remote) to make it work
    nvim_cmd(string.format('lua require(\'notify\')(\'%s\', \'%s\')', data.name, 'error'))
    -- make sure to use single quotes so vim can properly handle command.
  end,
}
